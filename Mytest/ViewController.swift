//
//  ViewController.swift
//  Mytest
//
//  Created by Iman Kazemini on 4/3/20.
//  Copyright © 2020 TestApp. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var mylable: UILabel!
    @IBOutlet weak var collectionview: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        self.collectionview?.register(CollectionViewCell.self, forCellWithReuseIdentifier: "CollectionViewCell")
        collectionview.delegate = self
        collectionview.dataSource = self
        self.collectionview.reloadData()
        
        
        let tab2 = UITapGestureRecognizer(target: self, action: #selector(lableTapped2(tapGestureRecognizer:)))
               mylable.isUserInteractionEnabled = true
               mylable.addGestureRecognizer(tab2)
        
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! CollectionViewCell
        
//        let tab2 = UITapGestureRecognizer(target: self, action: #selector(lableTapped(tapGestureRecognizer:)))
//        cell.mylable.isUserInteractionEnabled = true
//        cell.mylable.addGestureRecognizer(tab2)
//        
//        cell.mylable.backgroundColor = UIColor.red
        
        
        return cell
    }
    
    @objc func lableTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        print("ffffff1111")
    }
    
    @objc func lableTapped2(tapGestureRecognizer: UITapGestureRecognizer) {
           print("ffffff22222")
       }
    
}

